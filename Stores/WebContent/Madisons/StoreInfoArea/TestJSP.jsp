<%--
 =================================================================
  Licensed Materials - Property of IBM

  WebSphere Commerce

  (C) Copyright IBM Corp. 2006, 2009 All Rights Reserved.
 =================================================================
--%>
<%-- 
  *****
  * This JSP displays specials for the Madisons store.
  *****
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://commerce.ibm.com/base" prefix="wcbase" %>
<%@ taglib uri="flow.tld" prefix="flow" %>
<%@ taglib uri="http://commerce.ibm.com/coremetrics"  prefix="cm" %>
<%@ include file="../include/JSTLEnvironmentSetup.jspf" %>
<%@ include file="../include/nocache.jspf" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="${shortLocale}" xml:lang="${shortLocale}">
	<head>
		<title>Test JSP</title>
		<link rel="stylesheet" href='<c:out value="${jspStoreImgDir}${vfileStylesheet}"/>' type="text/css"/>
		<script type="text/javascript" src="<c:out value="${dojoFile}"/>" djConfig="${dojoConfigParams}"></script>
		<script type="text/javascript" src="<c:out value="${jsAssetsDir}javascript/MessageHelper.js"/>"></script>
		<%@ include file="../include/CommonJSToInclude.jspf"%>
	</head>

	<body>
	<%@ include file="../include/StoreCommonUtilities.jspf"%>
	
		<div id="page">
		
			<!-- Header Nav Start -->
			<%@ include file="../include/LayoutContainerTop.jspf"%>
			<!-- Header Nav End -->
			
			<%@ include file="../include/MessageDisplay.jspf"%>
		
			<!--MAIN CONTENT STARTS HERE-->
			<div id="content_wrapper_border">
			  This is a test JSP.
			</div>
			<!-- MAIN CONTENT END -->
		
			<!-- Footer Start -->
			<%@ include file="../include/LayoutContainerBottom.jspf"%>
			<!-- Footer End -->
		</div>
		<div id="page_shadow" class="shadow"></div>
<flow:ifEnabled feature="Analytics"><cm:pageview/></flow:ifEnabled>
	</body>
</html>
